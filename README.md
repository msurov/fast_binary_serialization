The serializer allows to pack/unpack simple data types: int, std::string, char*, float, etc.

Example of serialization:

```cpp
  char buf[1024];
  ser::pack(buf, sizeof(buf), "x", 0.1, "y", 2, "z", "ok");

```

Example of deserialization:
```cpp
  double x;
  int y;
  std::string z;

  ser::unpack(buf, sizeof(buf), "y", y, "z", z, "x", x);
  cout << x << " " << y << " " << z << endl;
```
