#include <iostream>
#include <functional>
#include <sstream>
#include "serializer.h"
#include "tcp.h"
#include "traces.h"
#include "common.h"

using namespace std;


int main(int argc, char *argv[])
{
    try
    {
        auto&& con = Connection::connect("127.0.0.1", 11005);
        auto&& writer = [con](char const* data, int datasz) { return con->write(data, datasz); };

        auto&& reader = [con](char* data, int datasz) { return con->read(data, datasz, true); };
        auto pack_reader = ser::make_pack_reader(reader);

        int sum = 0;

        int status = ser::send_pack(writer, "cmd", "start");
        if (status <= 0)
        {
            err_msg("sending error");
            return -1;
        }

        auto t = get_time_usec();

        for (int i = 0; i < 1000000; ++i)
        {
            ser::Packet pack;
            int status = pack_reader->fetch_next(pack);
            if (status <= 0)
                break;

            int inc;
            status = pack.get("inc", inc);
            if (status <= 0)
                break;

            sum += inc;
        }

        t = get_time_usec() - t;
        cout << t / 1000000.0 << endl;

        cout << sum << endl;
    }
    catch (std::exception& e)
    {
        cout << e.what() << endl;
        return -1;
    }

    return 0;
}
