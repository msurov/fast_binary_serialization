#ifdef _UNIX
#include <sys/time.h>
#include <unistd.h>
#include <sys/ioctl.h>
#elif defined(_WIN32)

#endif
#include <stdlib.h>
#include <signal.h>
#include <ctype.h>
#include <time.h>
#include <iostream>
#include <fstream>
#include "common.h"
#include <sys/types.h>
#include <stdexcept>
#include <sys/stat.h>
#include <fcntl.h>
#include <errno.h>
#include <string.h>
#include <unordered_map>
#include "traces.h"


using namespace std;


int str2int(char const* s)
{
    int val = 0;
    bool sign = *s == '-';

    s += sign;

    while (isdigit(*s))
    {
        val *= 10;
        val += *s - '0';
        ++ s;
    }

    return sign ? -val : val;
}

int int2str(int val, char* s)
{
    if (val == 0)
    {
        *s = '0';
        ++s;
        *s = '\0';
        return 1;
    }

    int len = 0;
    bool sign = val < 0;
    val = sign ? -val : val;

    *s = '-';
    s += sign;
    len += sign;

    int k = 1000000000;

    while (val < k)
    {
        k /= 10;
    }

    while (k != 0)
    {
        *s = (val / k) % 10 + '0';
        ++s;
        ++len;
        k /= 10;
    }

    *s = '\0';
    return len;
}

int64_t __beginning_epoch_usec()
{
    static const int64_t t0 = __epoch_usec();
    return t0;
}


/*
 * SIGNALS
 */
static std::unordered_map<int, weak_ptr<signal_handler_t>> __handlers;

static void common_sig_handler(int sig)
{
    auto entry = __handlers.find(sig);

    if (entry == __handlers.end())
    {
        dbg_msg("no signal handler for ", sig);
        return;
    }

    auto f = entry->second.lock();
    if (f)
    {
        dbg_msg("processing signal ", sig);
        (*f)();
    }
    else
    {
        dbg_msg("signal handler for ", sig, " is no more active");
    }
}

void set_sig_handler(int sig, shared_ptr<signal_handler_t> ptr_handler)
{
    __handlers[sig] = ptr_handler;
    signal(sig, common_sig_handler);
}

void set_sigterm_handler(shared_ptr<signal_handler_t> ptr_handler)
{
    set_sig_handler(SIGTERM, ptr_handler);
}

void set_sigint_handler(shared_ptr<signal_handler_t> ptr_handler)
{
    set_sig_handler(SIGINT, ptr_handler);
}

#ifdef _UNIX
void set_sighup_handler(shared_ptr<signal_handler_t> ptr_handler)
{
    set_sig_handler(SIGHUP, ptr_handler);
}
#endif
