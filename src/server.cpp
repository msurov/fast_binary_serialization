#include <iostream>
#include <sstream>
#include "serializer.h"
#include "tcp.h"
#include "common.h"
#include "traces.h"

using namespace std;


void new_connection_established(shared_ptr<Connection> con)
{
    auto reader = [con](char* pbuf, int bufsz) { return con->read(pbuf, bufsz, true); };
    auto pack_reader = ser::make_pack_reader(reader);
    auto&& writer = [con](char const* data, int datasz) { return con->write(data, datasz); };

    ser::Packet packet;
    int status = pack_reader->fetch_next(packet);
    if (status <= 0)
    {
        err_msg("can't obtain command from client; closing connection;");
        return;
    }

    string cmd;
    status = packet.get("cmd", cmd);
    if (status <= 0)
    {
        err_msg("client incorrect request; closing connection;");
        return;
    }

    if (cmd != "start")
    {
        err_msg("expected 'start' command; closing connection;");
        return;
    }

    for (int i = 0; true; ++ i)
    {
        status = ser::send_pack(writer, "inc", i);
        if (status <= 0)
            break;
    }
}

int main(int argc, char *argv[])
{
    try
    {
        TCPSrv server(11005);
        bool stop = false;
        auto f = [&stop, &server]() { stop = true; server.stop(); };
        auto pf = make_shared<signal_handler_t>(f);
        set_sigint_handler(pf);

        while (!stop)
        {
            auto con = server.wait_for_connection();
            if (!con)
                continue;

            new_connection_established(con);
        }
    }
    catch (exception& e)
    {
        cout << e.what() << endl;
        return -1;
    }

    return 0;
}
