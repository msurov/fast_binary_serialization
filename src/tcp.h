#pragma once

#include <memory>
#include <string>
#include <tuple>
#include <vector>


class TCPSrv;
class Connection;


/*
 * Connection
 */
 
class Connection
{
private:
    friend class TCPSrv;
    int remote_sock;

    Connection(int remotre_sock);
    Connection(Connection const&);

public:
    Connection(Connection&&);

    ~Connection();

    int write(char const* s, int len);
    int write(std::string const& s);
    int write(std::vector<uint8_t> const& buf);

    int read(char* s, int len, bool blocking);
    int read(std::string& buf, bool blocking);
    int read(std::vector<uint8_t>& buf, bool blocking);

    int available();
    std::tuple<int, std::string> read(bool blocking);
    bool alive();

    static std::shared_ptr<Connection> connect(std::string const& ip, int port);
};


/*
 * tcp server
 */

class TCPSrv
{
private:
    int     port;
    int     srv_sock;

private:
    void init_server();
    void stop_server();

public:
    TCPSrv(int port);
    ~TCPSrv();

    std::shared_ptr<Connection> wait_for_connection();
    void run();
    void stop();
};
