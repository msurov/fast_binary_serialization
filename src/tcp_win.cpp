#include <sys/types.h>
#include <winsock2.h>

#include <string.h>
#include <iostream>
#include <stdexcept>

#include "tcp.h"
#include "traces.h"


using namespace std;

static void winsock_init()
{
    static bool initialized = false;
    if (initialized)
        return;

    WSADATA data;
    if (WSAStartup(MAKEWORD(2, 2), &data))
        throw runtime_error("WSA Initialization error");
}

inline int available_bytes(int sock)
{
    unsigned long bytes_available = 0;
    int status = ioctlsocket(sock, FIONREAD, &bytes_available);
    if (status < 0)
        return status;
    return bytes_available;
}

Connection::Connection(int remote_sock) :
    remote_sock(remote_sock)
{
}

Connection::Connection(Connection&& connection)
{
    remote_sock = connection.remote_sock;
    connection.remote_sock = -1;
}

Connection::~Connection()
{
    if (remote_sock != -1)
        closesocket(remote_sock);

    dbg_msg("Connection closed");
}

int Connection::write(char const* s, int len)
{
    WSABUF buffer;
    buffer.buf = (char*)s;
    buffer.len = len;
    DWORD sent;
    DWORD flags = 0;

    DWORD nonblocking = 1;
    int status = ioctlsocket(remote_sock, FIONBIO, &nonblocking);
    if (status)
    {
        err_msg("socket ioctl error");
        return -1;
    }

    status = WSASend(remote_sock, &buffer, 1, &sent, flags, NULL, NULL);
    if (status)
    {
        err_msg("sending error");
        return -1;
    }

    return len != sent ? -1 : 1;
}

int Connection::write(string const& s)
{
    return write(s.data(), int(s.size()));
}

int Connection::write(std::vector<uint8_t> const& buf)
{
    return write(reinterpret_cast<char const*>(buf.data()), int(buf.size()));
}

int Connection::read(char* s, int len, bool blocking)
{
    WSABUF buffer;
    buffer.buf = s;
    buffer.len = len;
    DWORD received = 0;
    DWORD flags = 0;

    DWORD nonblocking = blocking ? 0 : 1;
    int status = ioctlsocket(remote_sock, FIONBIO, &nonblocking);
    if (status)
    {
        err_msg("socket ioctl error");
        return -1;
    }

    status = WSARecv(remote_sock, &buffer, 1, &received, &flags, NULL, NULL);
    if (status)
    {
        if (WSAGetLastError() == WSAEWOULDBLOCK)
            return 0;

        err_msg("socket reading error");
        return -1;
    }

    return received;
}

tuple<int, string> Connection::read(bool blocking)
{
    char buf[1024];
    int len = read(buf, sizeof(buf), blocking);
    if (len < 0)
        return make_tuple(-1, "");

    if (len == 0)
        return make_tuple(0, "");

    return make_tuple(1, string(buf, len));
}

int Connection::read(std::vector<uint8_t>& buf, bool blocking)
{
    buf.resize(1024);
    int status = read(reinterpret_cast<char*>(&buf[0]), int(buf.size()), blocking);
    if (status < 0)
    {
        buf.resize(0);
        return status;
    }
    else if (status > 0)
    {
        buf.resize(status);
        return status;
    }
    else
    {
        return 0;
    }
}

int Connection::read(std::string& buf, bool blocking)
{
    buf.resize(1024);
    int status = read(&buf[0], int(buf.size()), blocking);
    if (status < 0)
    {
        buf.resize(0);
        return status;
    }
    else if (status > 0)
    {
        buf.resize(status);
        return status;
    }
    else
    {
        return 0;
    }
}

std::shared_ptr<Connection> Connection::connect(std::string const& ip, int port)
{
    winsock_init();

    dbg_msg("connecting to the server ", ip, ":", port, "..");
    int sock = (int)socket(AF_INET, SOCK_STREAM, IPPROTO_TCP);
    if (sock < 0)
        throw runtime_error("can't open socket");

    sockaddr_in addr;
    memset(&addr, 0, sizeof(addr));
    addr.sin_family = AF_INET;
    addr.sin_addr.s_addr = inet_addr(ip.c_str());
    addr.sin_port = htons(port);

    int res = ::connect(sock, (sockaddr const*)&addr, sizeof(addr));
    if (res < 0)
    {
        closesocket(sock);
        throw runtime_error("can't connect to host '" + ip + ":" + to_string(port) + "'");
    }

    dbg_msg("connected");

    return std::shared_ptr<Connection>(new Connection(sock));
}


/*
* TCPSrv
*/

TCPSrv::TCPSrv(int port) :
    port(port), srv_sock(-1)
{
    winsock_init();
    init_server();
}

TCPSrv::~TCPSrv()
{
    if (srv_sock >= 0)
    {
        shutdown(srv_sock, SD_BOTH);
        closesocket((SOCKET)srv_sock);
        srv_sock = -1;
    }
}

shared_ptr<Connection> TCPSrv::wait_for_connection()
{
    int res = listen(srv_sock, SOCK_STREAM);
    if (res)
        throw runtime_error(string("listen socket error") + strerror(errno) + " for socket " + to_string(srv_sock));

    sockaddr_in remote_addr;
    int addr_len = (int)sizeof(remote_addr);

    while (true)
    {
        int remote_sock = (int)accept(srv_sock, (sockaddr*)&remote_addr, &addr_len);
        if (remote_sock > 0)
        {
            shared_ptr<Connection> ptr;
            ptr.reset(new Connection(remote_sock));
            return ptr;
        }

        int err = WSAGetLastError();
        if (err == WSAEINTR)
            return nullptr;

        throw runtime_error("can't accept Connection");
    }
}

void TCPSrv::stop()
{
    if (srv_sock > 0)
        shutdown(srv_sock, SD_BOTH);

    closesocket(srv_sock);
    srv_sock = -1;
}

void TCPSrv::init_server()
{
    srv_sock = (int)socket(AF_INET, SOCK_STREAM, IPPROTO_TCP);
    if (srv_sock < 0)
        throw runtime_error("can't open socket");

    sockaddr_in localhost;
    memset(&localhost, 0, sizeof(localhost));
    localhost.sin_port = htons(port);
    localhost.sin_family = AF_INET;
    localhost.sin_addr.s_addr = INADDR_ANY;

    int res = ::bind(srv_sock, (sockaddr const*)&localhost, sizeof(localhost));
    if (res)
    {
        closesocket(srv_sock);
        srv_sock = -1;
        runtime_error("can't bind socket to localhost");
    }
}
